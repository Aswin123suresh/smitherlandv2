<section class="bg-light ">
	<div class="container">
        <h2 class="text-center">List of Colleges</h2>
        <div class="row">
            <?php
            for($i = 0; $i < count($colglist); $i++){
            ?>
            <div class="col-lg-4 col-md-4 col-sm-12 mt-3 text-center">
                <div class="card">
                        <img src="<?php echo base_url('assets/img/collegelogo/'. $colglist[$i]->colg_logo) ?>" class="colg-img card-img img-fluid">
                    <div class="card-body">
                        <h5 class="card-title"><b><?php echo $colglist[$i]->colg_name ?></b></h5>
                        <div class="module card-text">
                            <p class="collapse" id="collapseExample" aria-expanded="false"><?php echo $colglist[$i]->description ?></p>
                        </div>
                        <a class="btn btn-primary f-13" href="<?php echo base_url('college-detail/'. $colglist[$i]->colg_id ) ?>">Read More</a>
                    </div>
                </div>
            </div>
            <?php
            } 
            ?>
        </div>
	</div>
</section>
