<section class="home-wrap p-0" id="home" style="background-image:url(assets/img/student.jpg)">
    <div class="overlay"></div>
    <div class="container ">
        <div class="row no-gutters slider-text align-items-center">
            <div class="col-lg-7">
                <div class="text aos-init aos-animate mb-4" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000">
                    <span class="subheading">Find Out</span>
                    <h1 class="mb-2">The Best Colleges in Canada</h1>
                    <div class="w-md-75 w-100">
                        <div class="colg-scrh mb-4">
                            <form>
                                <div class="inner-form">
                                    <div class="input-field first-wrap">
                                        <input id="search" type="text" placeholder="Name of the course you looking for?" />
                                    </div>
                                    <div class="input-field second-wrap">
                                        <div class="input-select">
                                            <select data-trigger="" name="choices-single-defaul">
                                                <option placeholder="">Province</option>
                                                <option>Alberta</option>
                                                <option>British Columbia</option>
                                                <option>Manitoba</option>
                                                <option>New Brunswick</option>
                                                <option>Newfoundland & Labrador</option>
                                                <option>Northwest Territories</option>
                                                <option>Nova Scotia</option>
                                                <option>Nunavut</option>
                                                <option>Ontario</option>
                                                <option>Prince Edward Island</option>
                                                <option>Quebec</option>
                                                <option>Saskatchewan</option>
                                                <option>Yukon</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field third-wrap">
                                        <button class="btn-search" onclick="clickedhere()" type="button">SEARCH</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container welcome text-center">
        <h2 class="mt-5">Welcome to Smitherland Global Immigration</h2>
        <hr>
        <p>These universities in Canada have been numerically ranked based on their positions in the overall Global Universities rankings. Schools were evaluated based on their research performance and their ratings by members of the academic community around the world and within North America. These are the top global universities in Canada.<a href="<?php echo base_url('about-us') ?>">Read More...</a></p>
        <button class="btn btn-primary " data-toggle="modal" data-target="#register">Register</button>
    </div>
</section>

<section >
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <img class="img-fluid" src="http://smitherland.ca/upload/home.jpg" alt="">
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="widget-title">
                    <h4><b>We help you start a life and a career in Canada</b></h4>
                    <hr>
                </div>
                <p>Smitherland Global Immigration Services is a Licensed Canadian Immigration Consulting Firm (ICCRC-R515433), providing immigration services to skilled professionals and students. We bring skilled professionals, students and their family members to Canada and help people just like you to realize your dream of a better life in Canada. We have a unique understanding of immigration issues and how they affect people’s lives. We are dedicated to providing our services with consistent professionalism and fairness. We operate in strict accordance with the high standards established by the Immigration Consultants of Canada Regulatory Council (ICCRC).</p>
            </div>
        </div>
    </div>
</section>

<section >
    <div class="container">
        <div class="row">
            <div  class="col-lg-6 col-md-6  col-12 ">
            <div class="widget-title">
                    <h3 class="text-justify"><b>How is the Canada Education System?</b></h3>
                    <hr>
            </div>
                <p class="text-justify mt-3">Canada invests heavily in the educational system. According to the Organization for Economic Cooperation and Development, Canada is one of the world's best education performers, ranking in the top three countries in terms of public post-secondary education investment per capita (OECD).</p>
                <p class="text-justify">Confused about the intakes in Canada? And Unlike a single intake in Indian universities, Canadian Colleges and Universities offer three intakes, which are</p>
                <ul class="container ">
                    <li><b>Fall:</b> A popular intake among Indian students, The Fall intake starts in the month of September</li>
                    <li><b>Winter:</b>Starts in January; it’s best if you miss the September intake</li>
                    <li><b>Summer:</b>Available for limited programs and colleges, the Summer intake usually starts around April and May  </li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6  col-12">
                <img class="img-fluid" src="assets/img/canada-education.jpg" alt="">
            </div>
        </div>
    </div>
</section>

<section class="bg-blue">
    <div class="container">
        <h1>Enroll With Us</h1>
        <p>It's easy as 1, 2, 3</p>
        <div class="row no-gutters align-items-center">
            <div class="col-lg-4 col-sm-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="card-title card-heading">1</div>
                        <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="card-title card-heading">2</div>
                        <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="card-title card-heading">3</div>
                        <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section >
    <div class="container">
        <div class="row">
            <div  class="col-md-6  col-12 ">
                <img src="assets/img/health-insurance.jpg" class=" img-fluid">
            </div>
            <div class="col-md-6  col-12">
            <div class="widget-title">
                <h3 class="text-center "><b>Canada Health Insurance for International Students</b></h3>
            </div>
                <p class=" text-justify mt-3 ">Known for its government-funded healthcare systems, it is beneficial to obtain student health insurance in Canada. During their time of study in Canada, they are required to obtain and maintain their Health Insurance.</p>
                <p class="text-justify ">There are certain provinces in Canada that offer provincial health coverage to international students. This could be provided for free or premium.</p>
                <p class="text-justify ">In the cases of those provinces that aren’t offering OSHC, you are expected to purchase private insurance through institutions. If this isn’t available, you can subscribe to mandatory health plans.</p> 
            </div>
        </div>
    </div>
</section>

<section >
    <div class="container">
        <div class="row">
            <div  class="col-md-6  col-12 ">
            <div class="widget-title">
                <h3 class="text-justify"><b>Canada Student Visa Requirements</b></h3>
                <hr>
            </div>
                <p class="text-justify mt-3">A student visa in Canada is your key to opening the door to studying in the best universities in Canada. Below are the requirements for the study abroad Canada permit, to obtain a Canada student visa. The Canada student visa fees for the application<b> costs around $150 and the Canada student visa process may take up to 90 days</b>.</p>
                <p class="text-justify">The Canada student visa Process and requirements to study in Canada</p>
                <p class="text-justify ">In the cases of those provinces that aren’t offering OSHC, you are expected to purchase private insurance through institutions. If this isn’t available, you can subscribe to mandatory health plans.</p>
            </div>
            <div class="col-md-6  col-12">
                <img src="assets/img/student-visa.jpg" class=" img-fluid">
            </div>
        </div>
    </div>
</section>

<section class="testimonial text-center">
        <div class="container">
            <div class="heading white-heading">
                Some of Our Client Testimonials
            </div>
            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
             
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">
                            <img src="assets/img/team1.jpg" class="img-circle img-responsive" />
                            <br>
                            <h4>Jacob Jeo</h4>
                            <p>Lorem ipsum dolor sit amet consectetur elit. Ipsa voluptatum ipsum, laudantium minima aliquam porro? Iste,  Et, esta ah</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <img src="assets/img/team2.jpg" class="img-circle img-responsive" />
                            <br>
                            <h4>Clint Vamos</h4>
                            <p>Lorem ipsum dolor sit amet consectetur elit. Ipsa voluptatum ipsum, laudantium minima aliquam porro? Iste,  Et, esta ah </p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <img src="assets/img/team3.jpg" class="img-circle img-responsive" />
                            <br>
                            <h4>Zimla Zakrya</h4>
                            <p>Lorem ipsum dolor sit amet consectetur elit. Ipsa voluptatum ipsum, laudantium minima aliquam porro? Iste,  Et, esta ah </p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
</section>




        <script src="assets/vendor/js/choices.js"></script>
        <script>
        const choices = new Choices('[data-trigger]',
        {
            searchEnabled: false
        });

        </script>










