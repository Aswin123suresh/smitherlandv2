<section class="bg-light">
    <div class="container">
        <h2 class="p-3 text-center">Find A Course</h2>
        <P class="text-center">Canada University Ranking · Exams For Canada · International English Language Testing System [IELTS] · Test of English as a Foreign Language [TOEFL] </P>
        <hr>
        <div class="row">
            <?php
            for($i = 0; $i < count($courses); $i++){
            ?>   
            <div class="col-md-4">
                <div class="card row ">
                    <a class="p-0" href="<?php echo base_url('course-list/'.$courses[$i]->c_id) ?>">
                        <img src="assets/img/courses/<?php echo $courses[$i]->course_img ?>" class="img-responsive card-img">
                        <div class="card-img-overlay text-white d-flex justify-content-center align-items-end bg-shade">
                            <p><b><?php echo $courses[$i]->course_name ?></b></p>
                        </div> 
                    </a>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>

