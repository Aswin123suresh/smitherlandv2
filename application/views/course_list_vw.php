<section class="bg-light">
    <div class="container ">
        <h2 class="text-center">All Courses Under <?php echo $course_type ?></h2>
        <p class="text-muted text-center"><b><?php echo count($course_details); ?></b> course(s) found, please scroll to view more.</p>
        <div class="row">
            <?php
            for($i = 0; $i < count($course_details); $i++){
            ?>
            <div class="card mt-4 bg-aqua">
                <div class="col-md-12 p-3">
                    <h5 class="card-title"><b><?php echo $course_details[$i]->program ?></b></h5>
                    <p class="text-muted"><?php echo $course_details[$i]->program_level ?></p>
                    <div class="row">
                        <div class="col-lg-8">
                            <p><b>Duration:</b> <?php echo $course_details[$i]->length ?></p>
                        </div>
                        <div class="col-lg-4"> 
                            <p><b>Fees: </b><?php echo $course_details[$i]->tution ?></p>
                        </div>
                    </div>
                    <div class="text-right">
                        <a class="btn btn-primary f-13" href="<?php echo base_url('course-details/'.$course_details[$i]->cd_id) ?>">More Details</a>
                    </div>
                </div>
            </div>
            <?php
            } 
            ?>
        </div>
    </div>
</section>


