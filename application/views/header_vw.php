<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Smitherland</title>

        <link rel="shortcut icon" href="<?php echo base_url('assets/favicon.ico') ?>" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/apple-touch-icon.png') ?>" />

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <link rel="stylesheet" href="<?php echo base_url('assets/css/cssbox.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/css/choices.css') ?>">

    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg fixed-top navbar-blue bottom-shadow" id="navbar">
                <div class="container">
                    <a class="navbar-brand align-items-center" href="<?php echo base_url() ?>">
                        <img class="logo" src="<?php echo base_url('assets/logo.png') ?>" />
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span> Menu
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item"><a class="nav-link activeme" href="<?php echo base_url('home') ?>">Home</a></li>
                            <li class="nav-item"><a class="nav-link activeme" href="<?php echo base_url('about-us') ?>" >About</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('college-list') ?>">List of College</a></li>
                            <li class="nav-item"><a class="nav-link activeme"  href="<?php echo base_url('find-course') ?>">Find a Course</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo base_url('contact-us') ?>">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        
      