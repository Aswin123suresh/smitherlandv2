/*const navbar = document.querySelector('.navbar')

window.addEventListener('scroll', () => {
    const y = (window.scrollY || window.pageYOffset);
    navbar.style.backgroundColor = `rgb(255 255 255 / 0%)`
    if ((y / 10) > 0) {
        const [o] = [y * 10];
        navbar.style.backgroundColor = `rgb(255 255 255 / ${o}%)`;
        var elems = document.querySelector(".navbar");
        if (elems !== null) {
            elems.classList.add("bottom-shadow");
        }
    }
})*/

var d = new Date();
document.getElementById("copyyear").innerHTML = d.getFullYear().toString().substring(2);

$(window).on('load', function () {
    let cooki = getCookie('smitherland-cookie');
    if (cooki == null || cooki == '') {
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + 500 * 36000;
        now.setTime(expireTime);
        document.cookie = 'smitherland-cookie=ok;expires=' + now.toUTCString() + ';path=/';
        $('#register').modal('show');
    }
});

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}