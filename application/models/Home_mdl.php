<?php
class Home_mdl extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->load->database();
	}
    
    function getCollegeList() {
        $this->db->select('colleges.*');
        $this->db->from('colleges');
        $this->db->where('colleges.flag', 1);
        $query = $this->db->get('');
        if($query) {
			return $query->result();
        } else {
            return false;
        }
    }
    
    function collegeDetail($collegeid) {
        $this->db->select('colleges.*');
        $this->db->from('colleges');
        $this->db->where('colleges.colg_id', $collegeid);
        $query = $this->db->get('');
        if($query) {
			return $query->result();
        } else {
            return false;
        }
    }

    function getCollegeCourseList($colgid) {
        $this->db->select('course_details.*');
        $this->db->from('course_details');
        $this->db->where('course_details.colg_id', $colgid);
        $query = $this->db->get('');
        if($query){
			return $query->result();
        }else{
            return false;
        }
    }

    function getCourseDetails($courseid) {
        $this->db->select('course_details.*');
        $this->db->from('course_details');
        $this->db->where('course_details.cd_id', $courseid);
        $query = $this->db->get('');
        if($query){
			return $query->result();
        }else{
            return false;
        }
    }

    function courseList() {
        $this->db->select('course_details.*');
        $this->db->from('course_details');
        // $this->db->where('course_details.c_id', $courseid);
        $query = $this->db->get('');
        if($query){
			return $query->result();
        }else{
            return false;
        }
    }



    function getCourses() {
        $this->db->select('courses.*');
        $this->db->from('courses');
        $this->db->where('courses.flag', 1);
        $query = $this->db->get('');
        if($query) {
			return $query->result();
        } else {
            return false;
        }
    }

    function getCourseType($courseid) {
        $this->db->select('courses.course_name');
        $this->db->from('courses');
        $this->db->where('courses.c_id', $courseid);
        $query = $this->db->get('');
        if($query) {
			$result = $query->row();
			return $result->course_name;
        } else {
            return false;
        }
    }

    function getCourseList($courseid) {
        $this->db->select('course_details.*');
        $this->db->from('course_details');
        $this->db->where('course_details.c_id', $courseid);
        $query = $this->db->get('');
        if($query){
			return $query->result();
        }else{
            return false;
        }
    }
    

}