<!-- Modal -->
    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="d-flex align-items-center">
                            <div class="bg-light rounded-circle p-3">
                                <img class="pop-img" src="assets/img/smalllogo.jpg" alt="">
                            </div>
                            <h5 class="modal-title text-success m-3">Register Now</h5>
                        </div>
                    </div>
                    <button type="button" class="close mdl-btn" data-dismiss="modal" data-bs-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="fullName" placeholder="Full Name" required>
                                    <label for="fullName">Full Name</label>
                                </div>        
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="email" placeholder="Email address" required>
                                    <label for="email">Email address</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="mobile" placeholder="Mobile Number" required>
                                    <label for="mobile">Mobile Number</label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="city" placeholder="Current city" required>
                                    <label for="city">Current city</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-group">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="course" placeholder="Course intersted in" required>
                                        <label for="course">Course intersted in</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 mb-3">
                                <div class="form-group">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="college" placeholder="College intersted in" required>
                                        <label for="college">College intersted in</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer">
                            <div class="row w-100">
                                <div class="col-lg-10 col-md-9">
                                    <p>By submitting this form, you accept and agree to our <a href="#">Terms of Use.</a> </p>
                                    <p><a href="">Already Registered? Click Here To Login.</a></p>
                                </div>
                                <div class="col-lg-2 col-md-3">
                                    <button type="submit" class="btn btn-primary w-100 mt-4">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <footer>
            <div class="col-md-12">
                <div class="social-media">
                    <a href="#">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="footer-sub">
                    <a href="#">Terms of Use</a> . 
                    <a href="#">Privacy Policy</a>
                </div>
                <div class="copyright">
                    &copy; 2016-<span id="copyyear"></span> Smitherland Global Immigration Services
                </div>
            </div>
        </footer>

        

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>