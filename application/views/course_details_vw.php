<section class="bg-light">
    <div class="container">
        <div class="card  p-4">
            <div class="row mb-3">
                <div class="col-md-9 col-12"><h3><?php echo $course_details[0]->program ?></h3></div>
                <div class="col-md-3 col-12">
                    <div class="text-right"><b>Next Intake</b></br><?php echo $course_details[0]->intake ?></div>
                </div>
            </div>
            <div class="row">
                <p class="mb-0"><b>Program Level: </b> <?php echo $course_details[0]->program_level ?></p>
                <p><b>Duration: </b><?php echo $course_details[0]->length ?></p>
            </div>
            <div class="row">
                <h6><b>Program Discription</b></h6>
                <p class="text-muted" style="font-size: 14px;"><?php echo $course_details[0]->description ?></p> 
            </div>
            <hr>
            <div class="my-3">
                <h6 class="text-center"><b>Tution & Application Fees</b></h6>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" class="text-primary"> Fees</th>
                            <th scope="col" class="text-primary">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Application fees</th>
                            <td><?php echo $course_details[0]->application_fee ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Tution fees</th>
                            <td><?php echo $course_details[0]->tution ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="my-3">
                <h6 class="text-center"><b>Scores Required</b></h6>
                <p>Minimum Level of Education: <?php echo $course_details[0]->min_educ ?></p>
                <p class="ml-start">Minimum GPA: <?php echo $course_details[0]->min_gpa ?></p> 
                <p class="text-muted f-14 text-center">The student should submit the scores of entrance examination tests like IELTS, TOEFL…, etc.</p>
                <div class="container ">
                    <div class="row justify-content-center">
                        <div class="col-md-2 card text-center p-2">
                            <h2 class="mb-1"><?php echo $course_details[0]->min_overl ?></h2>
                            <p class="mb-0">Overall Score in <br><span class="text-primary"><b>IELTS</b></span></p>
                        </div>
                        <div class="col-md-2 card text-center p-2">
                            <h2 class="mb-1"><?php echo $course_details[0]->min_list ?></h2>
                            <p class="mb-0">Score in<br><span class="text-primary"><b>Listening</b></span></p>
                        </div>
                        <div class="col-md-2 card text-center p-2">
                            <h2 class="mb-1"><?php echo $course_details[0]->min_read ?></h2>
                            <p class="mb-0">Score in <br><span class="text-primary"><b>Reading</b></span></p>
                        </div>
                        <div class="col-md-2 card text-center p-2">
                            <h2 class="mb-1"><?php echo $course_details[0]->min_speak ?></h2>
                            <p class="mb-0">Score in <br><span class="text-primary"><b>Speaking</b></span></p>
                        </div>
                        <div class="col-md-2 card text-center p-2">
                            <h2 class="mb-1"><?php echo $course_details[0]->min_writ ?></h2>
                            <p class="mb-0">Score in <br><span class="text-primary"><b>Writing</b></span></p>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>



