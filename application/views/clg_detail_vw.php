<section class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="content" class="content content-full-width">
                    <div class="profile">
                        <div class="profile-header">
                            <div class="profile-header-cover">
                                <img class="w-100" src="<?php echo base_url('assets/img/'.$colgdata[0]->colg_banner) ?>" alt="">
                            </div>
                            <div class="profile-header-content">
                                <div class="profile-header-img">
                                    <img src="<?php echo base_url('assets/img/collegelogo/'.$colgdata[0]->colg_logo) ?>" alt="">
                                </div>
                                <div class="profile-header-info">
                                    <h4><?php echo $colgdata[0]->colg_name ?></h4>
                                    <p>Location</p>
                                    <br>
                                </div>
                            </div>
                            <ul class="profile-header-tab nav nav-tabs">
                                <li class="nav-item"><a href="#college-info" class="nav-link" data-toggle="tab">College Info</a></li>
                                <li class="nav-item"><a href="#college-courses" class="nav-link active show" data-toggle="tab">Courses</a></li>
                                <li class="nav-item"><a href="#college-photos" class="nav-link" data-toggle="tab">Galary Photos</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="profile-content">
                        <div class="tab-content p-0">
                            <div class="tab-pane fade" id="college-info">
                                <p class="text-muted" style="font-size: 14px;"><?php echo $colgdata[0]->description ?></p>
                            </div>

                            <div class="tab-pane fade" id="college-photos">
                                <div class="">
                                    <div class="col-lg-4 p-0">
                                        <div class="cssbox">
                                            <a id="image1" href="#image1">
                                                <img class="cssbox_thumb w-100" src="<?php echo base_url('assets/img/'.$colgdata[0]->photos) ?>">
                                                <span class="cssbox_full">
                                                    <img src="<?php echo base_url('assets/img/'.$colgdata[0]->photos) ?>">
                                                </span>
                                            </a>
                                            <a class="cssbox_close" href=""></a>
                                            <a class="cssbox_next" href="#image2">&gt;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade active show" id="college-courses">
                                <div class="">
                                    <div class="row">
                                        <?php
                                        for($i = 0; $i < count($course_details); $i++){
                                        ?>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="card mt-4 bg-aqua">
                                                <h5 class="card-title text-center mb-0 p-3"><?php echo $course_details[$i]->program ?></h5>
                                                <div class="card-body text-center">
                                                    <p class="duration"><?php echo $course_details[$i]->length ?></p>
                                                    <div class="row">
                                                            <div class="col-lg-6">
                                                                <p><b>Application Fees</b><br><span class="f-13"><?php echo $course_details[$i]->application_fee ?></span></p>
                                                            </div>
                                                            <div class="col-lg-6"> 
                                                                <p><b>Tution Fees</b><br><span class="f-13"><?php echo $course_details[$i]->tution ?></span></p>
                                                            </div>
                                                        </div>
                                                    <a class="btn btn-primary" href="<?php echo base_url('course-details/'.$course_details[$i]->cd_id) ?>">View Course Details</a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        } 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>