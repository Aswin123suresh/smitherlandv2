<section class="bg-light">
    <div class="container">
        <h2 class="p-3"><b>About Smitherland </b></h2>
        <br>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-12">
                <img src="assets/img/about.jpg" class="img-fluid">
            </div>
            <div class="col-md-6 col-sm-12 col-12">
                <div class="widget-title">
                    <p>Smitherland Global Immigration Services is a Licensed Canadian Immigration consulting Firm (ICCRC-R515433) providing immigration services to skilled professionals and students. We bring skilled professionals, students and their family members to Canada and help people just like you to realize your dream of a better life in Canada. We have a unique understanding of immigration issues and how they affect people’s lives. We are dedicated to providing our services with consistent professionalism and fairness. We operate in strict accordance with the high standards established by the Immigration Consultants of Canada Regulatory Council (ICCRC).</p>
                </div>
            </div>
        </div>
    </div>
</section>


