<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent :: __construct();
        $this->load->helper(array( 'url'));
      
        $this->load->model('Home_mdl');
    }

	public function index()	{
		$this->load->view('header_vw');
		$this->load->view('home_vw');
		$this->load->view('footer_vw');
	}

	public function about()	{
		$this->load->view('header_vw');
		$this->load->view('about_vw');
		$this->load->view('footer_vw');
	}

	public function collegeList()	{
		$resp['colglist'] = $this->Home_mdl->getCollegeList();
		$this->load->view('header_vw');
		$this->load->view('clg_list_vw', $resp);
		$this->load->view('footer_vw');
	}

	public function collegeDetail($colgid) {
		$resp['colgdata'] = $this->Home_mdl->collegeDetail($colgid);
		$resp['course_details'] = $this->Home_mdl->getCollegeCourseList($colgid);
		$this->load->view('header_vw');
		$this->load->view('clg_detail_vw', $resp);
		$this->load->view('footer_vw');
	}

	public function courseDetails($courseid) {
		$resp['course_details'] = $this->Home_mdl->getCourseDetails($courseid);
		$this->load->view('header_vw');
		$this->load->view('course_details_vw', $resp);
		$this->load->view('footer_vw');
	}

	public function findCourse() {
		$resp['courses'] = $this->Home_mdl->getCourses();
		$this->load->view('header_vw');
		$this->load->view('findcourse_vw', $resp);
		$this->load->view('footer_vw');
	}

	public function courseList($courseid) {
		$resp['course_type'] = $this->Home_mdl->getCourseType($courseid);
		$resp['course_details'] = $this->Home_mdl->getCourseList($courseid);
		//echo '<pre>'; print_r($resp); exit();
		$this->load->view('header_vw');
		$this->load->view('course_list_vw', $resp);
		$this->load->view('footer_vw');
	}
	
	public function contact() {
		$this->load->view('header_vw');
		$this->load->view('contact_vw');
		$this->load->view('footer_vw');
	}
	
	

	

}
